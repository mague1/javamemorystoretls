# Memorystore TLS Examples

## Using

The main branch only contains this README.
There are branches for all of the examples

## Go

### golang branch

Use go-redis driver to connect to Memorystore with a TLS certificate

## Python

### python branch

Use python driver to connect to Memorystore with a TLS certificate


## Java 

### lettuce branch

This shows how to connect to Memorystore using a connection pool with the [Lettuce](https://lettuce.io/) driver with TLS
If you are new to Java and Memorystore - the Lettuce library is the most actively developed and has great features.

Using a single async connection and disabling auto-flush can lead to race conditions, so be careful.

### jedis branch

This shows how to connect to Memorystore using a connection pool with the Jedis driver with TLS

### Java Caveats

Java keystore does not support all features at this time so we will not be verifying the certificate

## Creating a test Memorystore instance

```
gcloud redis instances create mytest --transit-encryption-mode=SERVER_AUTHENTICATION --size=10 --region=us-west1 --enable-auth
gcloud redis instances get-auth-string mytest --region=us-west1
```

